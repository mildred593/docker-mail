// vim: sts=4:sw=4:et
package main

import (
    "flag"
    "net/http"
    "encoding/base64"
    "net"
    "os"
    "log"
    "time"
    "fmt"
    "bufio"
    "io/ioutil"
    "strings"
)

const(
    usersFile string = "/var/mail/users"
    dovecotAuthClientSocketFile string = "/var/run/dovecot/auth-client"
)

type User struct {
    user  string
    pass  string
    admin bool
}

func checkAdminAccess(user, pass string) (*User, error) {
    conn, err := net.DialUnix("unix", nil, &net.UnixAddr{Name: dovecotAuthClientSocketFile, Net: "unix"});
    if err != nil {
        return nil, err
    }
    defer conn.Close();
    plainReq := base64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("\x00%s\x00%s", user, pass)))
    req := fmt.Sprintf("VERSION\t1\t1\n"+
        "CPID\t%d\n"+
        "AUTH\t1\tPLAIN\tservice=admin\tresp=%s\n", os.Getpid(), plainReq)
    conn.Write([]byte(req));

    scanner := bufio.NewScanner(conn);
    for scanner.Scan() {
        line := scanner.Text()
        args := strings.Split(line, "\t")
        if args[0] == "FAIL" && args[1] == "1" {
            return nil, nil
        } else if args[0] == "OK" && args[1] == "1" {
            res := &User{user: user, pass:pass, admin:false}
            for _, arg := range args[2:] {
                if arg == "admin=1" {
                    res.admin = true
                }
            }
            return res, nil
        }
    }

    return nil, nil
}

// parseBasicAuth parses an HTTP Basic Authentication string.
// "Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==" returns ("Aladdin", "open sesame", true).
func parseBasicAuth(auth string) (username, password string, ok bool) {
	if !strings.HasPrefix(auth, "Basic ") {
		return
	}
	c, err := base64.StdEncoding.DecodeString(strings.TrimPrefix(auth, "Basic "))
	if err != nil {
		return
	}
	cs := string(c)
	s := strings.IndexByte(cs, ':')
	if s < 0 {
		return
	}
	return cs[:s], cs[s+1:], true
}

func handler(w http.ResponseWriter, r *http.Request) {
    username, pass, ok := parseBasicAuth(r.Header.Get("Authorization"))
    user, err := checkAdminAccess(username, pass)
    if err != nil {
        w.WriteHeader(http.StatusInternalServerError)
        w.Write([]byte(err.Error()))
        return
    }
    if !ok || user == nil {
        w.Header().Add("WWW-Authenticate", `Basic realm="Authentication"`)
        w.WriteHeader(http.StatusUnauthorized)
        w.Write([]byte("Please authenticate"));
        return
    }

    if !user.admin {
        w.Write([]byte("As a normal user you can't do anything on the administration interfrace"))
        return
    }

    if r.RequestURI == "/restart" {
        log.Println("Restarting admin server");
        w.Header().Add("Location", "/");
        //w.WriteHeader(http.StatusTemporaryRedirect);
        //w.Write([]byte("Restarting admin server"));
        w.Write([]byte(`<!DOCTYPE html><p>Restarting... Go <a href="javascript:back()">back</a> or go <a href="./">root</a></p>`))
        go func(){
            time.Sleep(2);
            os.Exit(1);
        }();
        return
    }

    w.Write([]byte("Users:\n"));

    f, err := os.Open(usersFile);
    if err != nil {
        w.Write([]byte(err.Error()))
    } else {
        defer f.Close();
        content, err := ioutil.ReadAll(f)
        if err != nil {
            w.Write([]byte(err.Error()))
        } else {
            w.Write(content);
        }
    }

}

func main() {
    listen := flag.String("listen", ":80", "Listen address and port")
    flag.Parse()
    http.HandleFunc("/", handler)
    http.ListenAndServe(*listen, nil)
}


